#pragma once

#define ADRESS 0x08

//SCL
#define PIN_SCL 7
//SDA
#define PIN_SDA 6

//LED
#define PIN_LED_G 3
#define PIN_LED_B 4 
#define PIN_LED_R 5

//HALL
#define PIN_HALL 8

//DIR
#define PIN_DIR 0
//STEP
#define PIN_STEP 1

#define PIN_ENA 2
