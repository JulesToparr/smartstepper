#include <Arduino.h>
#include <Wire.h>

#include "pin.h"
#include "stepper.h"
#include "led.h"

void setup() { 
  //initStepper();
  initLed();

  Wire.pins(PIN_SDA, PIN_SCL);
  Wire.begin(ADRESS);
  
  //pinMode(PIN_HALL, INPUT_PULLUP);
  //enableStepper(true);
}

void loop() {
  blinkLed();
  //updateStepper();

}

void requestEvent(){
  //Wire.write(digitalRead(PIN_HALL));
}
