#include "led.h"
#include "pin.h"
#include "Arduino.h"

long lastBlink = 0;

bool ledGState = false;
bool ledRState = false;
bool ledBState = false;

void initLed(){
  pinMode(PIN_LED_R, OUTPUT);
  pinMode(PIN_LED_G, OUTPUT);
  pinMode(PIN_LED_B, OUTPUT);

  digitalWrite(PIN_LED_R, LOW);
  digitalWrite(PIN_LED_G, LOW);
  digitalWrite(PIN_LED_B, LOW);

  
  testLed();
}

void ledR(bool state){
  if(ledRState != state){
    digitalWrite(PIN_LED_R, state);
    ledRState = state;
  }
}

void ledG(bool state){
  if(ledGState != state){
    digitalWrite(PIN_LED_G, state);
    ledGState = state;
  }
}

void ledB(bool state){
  if(ledBState != state){
    digitalWrite(PIN_LED_B, state);
    ledBState = state;
  }
}

void testLed(){
  ledR(true);
  delay(1000);
  ledG(true);
  delay(1000);
  ledB(true);
  delay(2000);
  ledR(false);
  delay(1000);
  ledG(false);
  delay(1000);
  ledB(false);  
}

void blinkLed(){
  if(!ledGState){
    if(millis() - lastBlink > 2000){
      ledG(true);
      lastBlink = millis();
    }
  }else{
    if(millis() - lastBlink > 80){
      ledG(false);
      lastBlink = millis();
    }
  }
}
