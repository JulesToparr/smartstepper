#include "Stepper.h"
#include "pin.h"
#include "Arduino.h"

bool stepState = false;
long lastStep = 0;
long currentPos = 0;

bool enabled = false;
bool dir = false;

void initStepper(){
  pinMode(PIN_STEP, OUTPUT);
  pinMode(PIN_DIR, OUTPUT);
  pinMode(PIN_ENA, OUTPUT);

  digitalWrite(PIN_ENA, HIGH);
  digitalWrite(PIN_STEP, LOW);
  digitalWrite(PIN_DIR, LOW);
}

void enableStepper(bool state){
  if(enabled != state){
    pinMode(PIN_ENA, !state);
  }
}

void step(bool state){
  if(stepState != state){
    digitalWrite(PIN_STEP, state);
    stepState = state;
  }
}

void setInverseStepper(bool state){
  if(dir != state){
    pinMode(PIN_DIR, state);
  }
}

void updateStepper(){
  if(currentPos <= 1600)
    if(!stepState){
      if(micros() - lastStep > 200){
        step(true);
        lastStep = micros();
      }
    }else{
      if(micros() - lastStep > 10){
        step(false);
        currentPos++;
        lastStep = micros();
      }
    }
  if(currentPos >= 800)
    digitalWrite(PIN_ENA, HIGH);
}
