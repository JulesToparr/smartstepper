#pragma once

void step(bool state);
void initStepper();
void enableStepper(bool state);
void setInverseStepper(bool state);
void updateStepper();