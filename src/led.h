#pragma once

#define PIN_LED_G 3
#define PIN_LED_B 4 
#define PIN_LED_R 5

void initLed();

void ledR(bool state);

void ledG(bool state);

void ledB(bool state);

void testLed();

void blinkLed();